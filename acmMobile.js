// JavaScript Document

$(document).ready(function () {
    $("#nav").slideUp(0);

    $('#nav-btn').click(function () {
        $('#nav').slideToggle(200);
        $('#content').fadeToggle(200);
    });
});

//$(document).ready(function () {
//    var nav = "#nav";
//    var isDown = false;
//
//    $(nav).slideUp(0);
//
//    $('#nav-btn').click(function () {
//        if (isDown == false) {
//            $(nav).slideDown(200);
//            $('#content').hide();
//
//            isDown == true;
//        }
//        if (isDown == true) {
//            $(nav).slideUp(200);
//            $('#content').show();
//
//            isDown == false;
//        }
//    });
//});